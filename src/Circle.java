public class Circle extends Elliplse implements Move {

    public Circle(double radius1, int x, int y) {
        super(radius1, radius1, x, y);
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius1;
    }


    @Override
    public void moveFigure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circle с длиной окружности " + this.getPerimeter() + " и координатами (" + x + " , " + y + ")";
    }

}
