public class Elliplse extends Figure {

    protected double radius1;
    private double radius2;

    public Elliplse(double radius1, double radius2, int x, int y) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * (radius1 + radius2);
    }

    @Override
    public String toString() {
        return "Эллипс с длиной окружности " + this.getPerimeter() + " и координатами (" + x + " , " + y + ")";
    }
}
