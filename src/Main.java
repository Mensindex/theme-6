public class Main {

    public static void changeСoordinatesTo0(Move[] figures) {
        for (int i = 0; i < figures.length; i++) {
            figures[i].moveFigure(0, 0);
            System.out.println(figures[i].toString());
        }
    }

    public static void main(String[] args) {


        Elliplse elliplse = new Elliplse(5, 9, 0, 0);
        Rectangle rectangle = new Rectangle(5, 9, 0, 0);


        Square square1 = new Square(5, -2, -13);
        Square square2 = new Square(7, 9, 10);
        Circle circle1 = new Circle(5, 6, 10);
        Circle circle2 = new Circle(7, -8, 4);


        Move[] figures = new Move[4];
        figures[0] = square1;
        figures[1] = square2;
        figures[2] = circle1;
        figures[3] = circle2;


        System.out.println(elliplse.toString());
        System.out.println(rectangle.toString());
        System.out.println();
        System.out.println("Массив:");
        changeСoordinatesTo0(figures);
    }
}
