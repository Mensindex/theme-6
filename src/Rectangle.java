public class Rectangle extends Figure {

    protected double a;
    private double b;

    public Rectangle(double a, double b, int x, int y) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return (a + b) * 2;
    }

    @Override
    public String toString() {
        return "Прямоугольник с периметром " + this.getPerimeter() + " и координатами (" + x + " , " + y + ")";
    }
}
