public class Square extends Rectangle implements Move{

    protected double a;
    protected int x;
    protected int y;

    public Square(double a, int x, int y) {
        super(a, a, x, y);
        this.a = a;
        this.x = x;
        this.y = y;
    }

    @Override
    public double getPerimeter() {
        return a * 4;
    }

    @Override
    public void moveFigure(int x, int y) {
        this.x=x;
        this.y=y;
    }


    @Override
    public String toString() {
        return "Квадрат с периметром " + this.getPerimeter() + " и координатами (" + x + " , " + y + ")";
    }

}
